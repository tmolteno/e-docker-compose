# e-docker-compose

A simple docker-compose script for automatically building the http://www.e.foundation android distribution.

## Build Instructions

Ensure you have around 150GB of free space on your hard drive, and at least 8 GB or RAM. The first build wil take a long time (12 hours) while subsequent builds will only take an hour or so.

### Customising the devices to build
Modify the line:
    DEVICE_LIST: 'oneplus2'
to refer to your device, or alternatively if you'd like to build several, make a comma separated list. These are code names for devices, and you can find your devices [https://wiki.lineageos.org/devices/].

### Buiding only once
Change the lines:
            CRONTAB_TIME: '0 12 * * *'
        restart: always
to
            CRONTAB_TIME: now
        restart: no
and the buildbot will build immediately and exit.

## Start the Buildbot

    docker-compose up -d

This will download and sync the repositories and build the android distribution. This takes a long time. Tens of hours. If you omit the '-d' then the compilation will occur in the foreground. 

By default, the docker-container will wait until 12:00 UTC and then build the android.


## Subsequent builds

    docker-compose up -d
    
This will resync repositories and should only take an hour or so.

## Location of your Android Build

This is stored in /e/root/zips


## Update OTA Location

The environment variable OTA_URL should point to the buld server itself (it acts as an OTA API). If your build server
is http://my.ota.uri/, then you would put OTA_URL: http://my.ota.uri/api
